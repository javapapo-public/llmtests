import sys
from llama_index.llms.ollama import Ollama

MODEL = "llama2"


if __name__ == "__main__":
    print("Starting")
    sys
    llama = Ollama(model=MODEL, request_timeout=300)
    print(f"loaded {MODEL}")
    resp = llama.complete("Who is Paul Graham?")
    print(resp)
