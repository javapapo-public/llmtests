from llama_index.core import SummaryIndex, Settings
from llama_index.llms.ollama import Ollama
from llama_index.readers.web import SimpleWebPageReader
from llama_index.readers.web import BeautifulSoupWebReader

URLS = ["https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/"]

if __name__ == "__main__":
    reader = BeautifulSoupWebReader()
    documents = reader.load_data(urls=URLS)
    print(documents)
    print('Done')
