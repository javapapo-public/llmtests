import sys

from llama_index.core import Settings
from llama_index.core import SimpleDirectoryReader, StorageContext
from llama_index.core import VectorStoreIndex
from llama_index.core.indices.loading import load_index_from_storage
from llama_index.embeddings.huggingface import HuggingFaceEmbedding
from llama_index.llms.ollama import Ollama

import os

EMBEDDING_MODEL = "BAAI/bge-small-en-v1.5"
LLM_MODEL = "llama2"
PERSIST_DIR = "/home/papo/dev/llm/output"


def is_folder_empty(folder_path: str) -> bool:
    folder = os.listdir(folder_path)
    result = False
    # Checking if the list is empty or not
    if len(folder) == 0:
        result = True
    return result


def load_file(file_path: str):
    loader = SimpleDirectoryReader(input_files=[file_path])
    return loader.load_data(show_progress=True)


Settings.embed_model = HuggingFaceEmbedding(model_name=EMBEDDING_MODEL)
Settings.llm = Ollama(model=LLM_MODEL, request_timeout=300.0)

if __name__ == "__main__":

    print("Starting")
    file_to_load = sys.argv[1]
    print(f"File: {file_to_load}")
    # embedding_model = HuggingFaceEmbedding(model_name=EMBEDDING_MODEL)
    # print('loaded embedding_model')
    if is_folder_empty(PERSIST_DIR):
        documents = load_file(file_to_load)
        index = VectorStoreIndex.from_documents(documents, show_progress=True)
        index.storage_context.persist(persist_dir=PERSIST_DIR)
    else:
        # load the existing index
        storage_context = StorageContext.from_defaults(persist_dir=PERSIST_DIR)
        index = load_index_from_storage(storage_context)

        # either way we can now query the index
    query_engine = index.as_query_engine(llm=Settings.llm)
    print(query_engine.query("What is the latest version of SpringBoot?"))
