from llama_index.core import SummaryIndex, Settings
from llama_index.llms.ollama import Ollama
from llama_index.readers.web import SimpleWebPageReader


if __name__ == "__main__":
    llm = Ollama(model="llama3", request_timeout=60.0)
    response = llm.complete("Do you understand Greek can you say hello how are you?")
    print(response)
