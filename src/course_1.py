from llama_index.core import SummaryIndex, Settings
from llama_index.llms.ollama import Ollama
from llama_index.readers.web import SimpleWebPageReader

URLS = ["https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/"]
LLM_MODEL = "llama3"
LLM = Ollama(model="llama3", request_timeout=300.0)
Settings.llm = LLM

if __name__ == "__main__":
    docs = SimpleWebPageReader(html_to_text=True).load_data(urls=URLS)
    index = SummaryIndex.from_documents(docs)
    query_engine = index.as_query_engine()
    response = query_engine.query("What is the latest version of SpringBoot?")
    print(response)
